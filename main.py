from aws import get_resource
from utils import write_in_s3

import pandas as pd

key_file = 'data.csv'
df = pd.read_csv('data.csv')

write_in_s3(get_resource(), 'lambda-test-armindo', df, key_file)