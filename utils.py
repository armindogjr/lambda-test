import pandas as pd
from io import StringIO 
import boto3
from config import passwd, id

def get_resource():

    # Creating the high level object oriented interface
    resource = boto3.resource(
        's3',
        aws_access_key_id = id,
        aws_secret_access_key = passwd,
        region_name = 'us-east-2'
    )

    return resource

def create_bucket(bucket_name, region=None):
    """Create an S3 bucket in a specified region

    If a region is not specified, the bucket is created in the S3 default
    region (us-east-1).

    :param bucket_name: Bucket to create
    :param region: String region to create bucket in, e.g., 'us-west-2'
    :return: True if bucket created, else False
    """

    # Create bucket
    try:
        if region is None:
            s3_client = boto3.client('s3')
            s3_client.create_bucket(Bucket=bucket_name)
        else:
            s3_client = boto3.client('s3', region_name=region)
            location = {'LocationConstraint': region}
            s3_client.create_bucket(Bucket=bucket_name,
                                    CreateBucketConfiguration=location)
    except ClientError as e:
        logging.error(e)
        return False
    return True


def write_in_s3(local_resource: None, bucket_name: str, df: pd.DataFrame, key_file: str,) ->  None:

    if bucket_name is None:
        create_bucket(bucket_name, region=None)
    else:
        csv_buffer = StringIO()
        df.to_csv(csv_buffer)
        local_resource.Object(bucket, key_file).put(Body=csv_buffer.getvalue())

    return print(f'{key_file} escrito no bucket {bucket}')